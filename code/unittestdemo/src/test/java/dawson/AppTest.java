package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void shouldAnswerWith5() {
        App a = new App();
        assertEquals("it worked", 5, a.echo(5));
    }

    @Test
    public void shouldAnswer6() {
        App a = new App();
        assertEquals("it worked", 6, a.oneMore(5));
    }
}
